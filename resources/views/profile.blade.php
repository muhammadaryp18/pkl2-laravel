<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Profile</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('lte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
  <link rel="shortcut icon" href="{{ asset('lte/dist/img/foto/logo.ico') }}" type="image/x-icon">
</head>
<body class="hold-transition dark-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">

  

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('lte/dist/img/foto/logo.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Pare</a>
        </div>
      </div>


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item ">
               <a href="index" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Beranda
              </p>
            </a>
          </li>
          <li class="nav-item menu-open">
            <a href="profile" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Profile
              </p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="gallery" class="nav-link">
              <i class="nav-icon far fa-image"></i>
              <p>
                Gallery
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="info" class="nav-link">
              <i class="nav-icon fas fa-columns"></i>
              <p>
                Info
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="contactus" class="nav-link">
              <i class="nav-icon far fa-envelope"></i>
              <p>
                Contact Us
              </p>
            </a>
            
          
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Profie</h1>
          </div><!-- /.col -->
          
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="about" data-aos="fade-up">
      <div class="container">

        <div class="row">
          <div class="col-lg-6">
          
          <img src="{{ asset('lte/dist/img/foto/welcome.jpeg') }}" class="img-fluid" alt="" >
            
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            <h3>
            Sejarah dan Budaya</h3>
            <p class="fst-italic">
Kecamatan Pare diproduksi menjadi terkenal di seluruh alam karena di sinilah antropolog kaliber alam, Clifford Geertz - yang kala itu masih diproduksi menjadi mahasiswa doktoral - melakukan penelitian lapangannya yang kemudian ditulisnya sebagai sebuah buku yang berjudul The Religion of Java. Dalam buku tersebut Geertz menyamarkan Pare dengan nama "Mojokuto". Di Pare, antropolog ini sering meminta keterangan dan berkonsultasi dengan Bapak S. Sunuprawiro (alm), waktu itu diproduksi menjadi wartawan Jawa Pos. Pak Sunu adalah salah satu narasumber yang menolong antropolog tersebut dalam menyelesaikan bukunya.

Pare termasuk kota lama. Ini terbukti dari keberadaan dua candi tidak jauh dari pusat kota, yakni Candi Surowono dan Candi Tegowangi, serta keberadaan patung "Budo" yang benar tepat di pusat kota. Ketiga peninggalan ini membuktikan bahwa Pare telah lahir ratusan tahun lalu. Dahulu di Pare terdapat jalur kereta api dari Kediri ke Jombang, tetapi sekarang hanya tersisa relnya saja. Hanya sampai sekarang belum dikenal dengan pasti kapan kota Pare berdiri dan siapa pendirinya.

          </div>
        </div>

      </div>
    </section><!-- End About Section -->
		<section>
                
<h3>Lokasi</h3>
<p >
Pare terletak 25 km sebelah timur laut Kota Kediri, 
atau 120 km barat daya Kota Surabaya.
 Pare benar pada jalur Kediri-Malang dan jalur Jombang-Kediri serta Jombang - Blitar. Sudah lama benar wacana Pare dikembangkan diproduksi menjadi ibu kota Kabupaten Kediri, yang secara berangsur-angsur dipindahkan dari Kota Kediri. Namun niat ini tidak pernah serius dilakukan oleh Pemerintah Kabupaten atau para Bupati yang menjabat. (mulai era Bupati H. Sutrisno, Wacana tersebut belakangnya benar-benar dibatalkan, karena akan mendapatkan protes dari warga di beberapa wilayah Kabupaten Kediri, terutama di daerah selatan-seperti Kras, Ngadiluwih, Kandat dan Ringinrejo dan di daerah barat sungai Brantas-seperti tarokan, Grogrol, Banyakan, semen dan Mojo. Sehingga diambil jalan tengah dengan menempatkan Pusat pemerintahan di wilayah Kec. Ngasem Kediri, tepatnya di Ds. Sukorejo (biasa dinamakan Katang) dan akan juga didirikan Pusat Bisnis di Wilayah Kota Baru Gumul.)
</p>

<h3>Kondisi lingkungan</h3>
<p>Kota Pare yang benar pada ketinggian 125 meter di atas permukaan 
laut ini benar udara yang tidak terlalu panas.
 Bermacam jenis jajanan dan makanan enak dan higinis dengan 
 harga "kampung" bisa dijumpai dengan mudah di kota kecil ini. Bermacam infrastruktur dan fasilitas kehidupan kota juga dengan mudah bisa dijumpai: hotel, rumah sakit (yang agung HVA dan RSUD rumah bersalin yang lengkap pun juga ada), ATM bersama, warnet 24 jam ber-AC, masjid, dsb-nya.

Pare adalah kota adipura. Sekolah-sekolah favorit banyak 
berdiri di kota pare ini dari tingkat TK sampai dengan SMA. Seperti SMP Negeri 2 Pare yang adalah sekolah bertaraf internasional. Pada tangkat SMA terdapat SMA Negeri 1 Pare dan SMA Negeri 2 Pare, dan juga benar MA Negeri Krecek.

</p>

<h3>Ekonomi</h3>
<p>Pare benar tanah yang subur bekas letusan gunung Kelud dan tidak pernah merasakan kekeringan. Produk agraria andalan dari Pare adalah bawang merah, biji mente dan melinjo. Sedangkan oleh-oleh khas dari Pare selang lain adalah kenal kuning dan gethuk pisang. Di Pare sudah lama muncul berturut-turut industri menengah bertaraf internasional, seperti industri plywood dan pengembangan bibit-bibit pertanian. Tempat-tempat rekreasi pun telah benar semenjak tahun 1970-an meskipun sederhana, seperti Pemandian "Canda-Bhirawa" Corah dan alun-alun "Ringin Budo"serta sentra ikan hias di dsn Surowono Desa Canggu.
</p>
</section>
        <!-- Info boxes -->
                  <!-- /.item -->
                </ul>
              </div>
              <!-- /.card-body -->
              
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong></strong>
    <div class="float-right d-none d-sm-inline-block">
     
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ asset('lte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('lte/dist/js/adminlte.js') }}"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ asset('lte/plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('lte/plugins/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('lte/plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
<script src="{{ asset('lte/plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('lte/plugins/chart.js/Chart.min.js') }}"></script>

<!-- AdminLTE for demo purposes -->
<script src="{{ asset('lte/dist/js/demo.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('lte/dist/js/pages/dashboard2.js') }}"></script>
</body>
</html>
